import { Resolve } from "@angular/router";
import { Injectable } from "@angular/core";
import { ProduitService } from "./produit.service";

@Injectable()
export class ProduitResolver implements Resolve<any>{

    constructor(private produitService:ProduitService){

    }

    resolve(){
        return this.produitService.getProduits();
    }
}