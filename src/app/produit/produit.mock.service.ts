
import { Produit } from '../shared/produits';
import { Injectable } from '@angular/core';

@Injectable()
export class ProduitMockService{
  private PRODUITS : Produit []=[];

  constructor(){
    let p1:Produit= new Produit(1,'Livre',100,10);
    let p2:Produit= new Produit(2,'cahier',200,5);

    this.PRODUITS.push(p1);
    this.PRODUITS.push(p2);
  }

  public getProduits(): Produit[]{
    return this.PRODUITS;
  }
}
